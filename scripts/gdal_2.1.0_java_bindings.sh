#!/bin/sh
set -e

# make the java bindings
echo 'Making SWIG bindings for Java...';
cd gdal-2.1.0/swig/java;
cat java.opt | sed "s|# Java Stuff|JAVA_HOME = $JAVA_HOME|" \
    > java.opt.new && mv java.opt.new java.opt

# Clean out possible lingering CPP files for SWIG and the make java bindings
make veryclean 
make -I$JAVA_HOME/include -I$JAVA_HOME/include/linux
make install

cd /usr/lib/x86_64-linux-gnu
ln -s libproj.so.9.1.0 libproj.so
