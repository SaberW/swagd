SWAGD : Software to Aggregate Geospatial Data
===============

A Java-based application for converting geospatial images into tiles, packaging those tiles, and viewing them on a map.

[![build status](https://gitlab.com/GitLabRGI/swagd/badges/master/build.svg)](https://gitlab.com/GitLabRGI/swagd/commits/master)
[![coverage report](https://gitlab.com/GitLabRGI/swagd/badges/master/coverage.svg)](https://gitlab.com/GitLabRGI/swagd/commits/master)
[![Maven Central](https://maven-badges.herokuapp.com/maven-central/com.rgi-corp/swagd/badge.svg)](https://maven-badges.herokuapp.com/maven-central/com.rgi-corp/swagd)

## Projects
* __Common__: Geospatial functionality dependency
  * _Dependency_: Nothing
* __DataStore__: Functionality responsible for manipulating tile stores
  * _Dependency_: Common, GeoPackage
* __Gdal2Tiles__: Functionality necessary for converting raster imagery into tiles
  * _Dependencies_: Common, GDAL (refer to build environment wiki)
* __Geopackage__: Functions and validators for creating or reading GeoPackages
  * _Dependency_: Common
* __Packager__: Workflow code that will use the Geopackage library to create Geopackage data products
  * _Dependencies_: Common, GeoPackage